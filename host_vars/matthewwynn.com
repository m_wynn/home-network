---
network_input_policy: ACCEPT
network_output_policy: ACCEPT

xbps_repository_main: https://lug.utdallas.edu/mirror/void/current
xbps_repository_nonfree: https://lug.utdallas.edu/mirror/void/current/nonfree
xbps_repository_multilib: https://lug.utdallas.edu/mirror/void/current/multilib
xbps_repository_multilib_nonfree: https://lug.utdallas.edu/mirror/void/current/multilib/nonfree
xbps_repository_address: lug.utdallas.edu/mirror/void
xbps_repository_port: 443

jekyll_sites:
- site_name: matthewwynn.com
  site_path: /var/www/home
  site_url: matthewwynn.com
  source_url: https://github.com/m-wynn/matthewwynn.com
  source_host: github.com
  source_port: 443
  ssl_certificate_path: /var/lib/acme/live/matthewwynn.com/fullchain
  ssl_key_path: /var/lib/acme/live/matthewwynn.com/privkey
  use_ssl: true
  webserver: nginx
  ssl_stapling: yes
  ssl_hsts_age: 15768000


acmetool:
  sites:
    - site: matthewwynn.com
      names:
        - matthewwynn.com
  hostmaster_email: matthew@matthewwynn.com
